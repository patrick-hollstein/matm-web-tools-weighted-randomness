/** ////////////////////
 *
 * rdm_weighted()
 * 
 * Returns a random array index. The random factor is weighted by the integers of the array.
 * See @example.
 *
 * @param {Number[]} weights Array of integers that define the weights.
 *
 * @returns {Number} Returns an Integer between 0 and @param weights.length - 1.
 *
 * @example rdm_weighted([10, 150, 40]); // Returns the value 0 with a probability of 5%, the value 1 with 75% and the value 2 with 20%.
 *
 * /////////////////////
*/

function rdm_weighted(weights) {
	// sum weights
	let sum = weights.reduce(function (a, b) { return a + b; });

	// Generate random number in the range [0, sum].
	let rdm = Math.round(Math.random() * sum);

	// Determine the result of the weighted random
	let weights_length = weights.length;
	for (let i = 0; i < weights_length; ++i) {
		// Subtract the weight from the random number
		rdm -= weights[i];

		// As soon as random - weight <= 0, one knows that the random value is in the range of the weight
		if (rdm <= 0) {
			return i;
		}
	} // for i
} // rdm_weighted()