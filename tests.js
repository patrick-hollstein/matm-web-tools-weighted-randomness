function test1() {
	let arr = [10, 150, 40];
	let counter = [0, 0, 0];

	for (let i = 0; i < 100; i++) {
		let rdm = rdm_weighted(arr);

		counter[rdm]++;
	} // for i

	document.getElementById('td0').innerHTML = counter[0] + '%';
	document.getElementById('td1').innerHTML = counter[1] + '%';
	document.getElementById('td2').innerHTML = counter[2] + '%';
} // test()


function test2() {
	let arr = [33, 74, 17, 0, 81];
	let counter = [0, 0, 0, 0, 0];

	for (let i = 0; i < 100; i++) {
		let rdm = rdm_weighted(arr);

		counter[rdm]++;
	} // for i

	document.getElementById('td-0').innerHTML = counter[0] + '%';
	document.getElementById('td-1').innerHTML = counter[1] + '%';
	document.getElementById('td-2').innerHTML = counter[2] + '%';
	document.getElementById('td-3').innerHTML = counter[3] + '%';
	document.getElementById('td-4').innerHTML = counter[4] + '%';
} // test()


test1();
test2();